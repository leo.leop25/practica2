/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Leonardo Peralta 
 */
public class Lista <E>{
    
    private E dato;
    private Lista <E> siguiente;
    
    public Lista(E dato, Lista<E> siguiente){
        this.dato=dato;
        this.siguiente=siguiente;
    }
    
    public Lista(){
        this.dato= null;
        this.siguiente=siguiente;                
    }
    public E getDato() {
        return dato;
    }

    public void setDato(E dato) {
        this.dato = dato;
    }

    public Lista<E> getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(Lista<E> siguiente) {
        this.siguiente = siguiente;
    }
}
