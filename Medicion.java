
/**
 *
 * @author Leonardo Peralta
 */
import org.github.jamm.MemoryMeter;
public class Medicion <E> {
    
    private Lista<E> cabecera;
    private Integer size;
    

    public static void main(String[] args){
        
        MemoryMeter medidor = MemoryMeter.builder().build();

        byte bait=127;
        short corto=32767;
        int entero=2147483647;
        long largo=922337999;
        float flotante=354565499;
        double dovle=1349345554;
        char chart=34499;
        String Vacio = "";
        String Lleno= " encebollado con chifle ";
               
        String cadena1[]= new String[]{Lleno};
        byte cadena2[]= new byte[157];
        short cadena3[]= new short[45];
        int cadena4[]= new int[]{123443,123,3432123,25233,5545};
        long cadena5[]= new long[]{123443,123,3432123,25233,5545};
        float cadena6[]= new float[45];
        double cadena7[]= new double[]{123443,123,3432123,25233,5545};
        char cadena8[]= new char[]{chart};
  
        
        System.out.println("Sring Vacio>> "+Vacio+">> "+medidor.measureDeep(Vacio));
        System.out.println("String Lleno>> "+Lleno+">> "+medidor.measureDeep(Lleno));
        System.out.println("Byte>> "+bait+">> "+medidor.measureDeep(bait));
        System.out.println("Short>> "+corto+">> "+medidor.measureDeep(corto));
        System.out.println("Entero>> "+entero+">> "+medidor.measureDeep(entero));
        System.out.println("Long >>"+largo+">> "+medidor.measureDeep(largo));
        System.out.println("Flotante >>"+flotante+">> "+medidor.measureDeep(flotante));
        System.out.println("Double >>"+dovle+">> "+medidor.measureDeep(dovle));
        System.out.println("Caracter >>"+chart+">> "+medidor.measureDeep(chart));
        
        System.out.println("\nstring >> "+medidor.measureDeep(cadena1));
        System.out.println("byte >> "+medidor.measureDeep(cadena2));
        System.out.println("short >> "+medidor.measureDeep(cadena3));
        System.out.println("int >> "+medidor.measureDeep(cadena4));
        System.out.println("long >>"+medidor.measureDeep(cadena5));
        System.out.println("float >> "+medidor.measureDeep(cadena6));
        System.out.println("double >> "+medidor.measureDeep(cadena7));
        System.out.println("char >> "+medidor.measureDeep(cadena8));
        
        
        Medicion <Integer> lista = new Medicion();
        lista.insertar(67);
        lista.insertar(6);
        lista.insertar(16); /// para las posiciones ojoooooo
        
        lista.imprimir();
        System.out.println("\nLista >> "+medidor.measureDeep(lista.cabecera ));
       
    }
    public Medicion(){
        cabecera = null;
        size=0;
    }
    
    public boolean Vacia(){
        return cabecera == null;
    }
    public Integer getSize(){
        return size;
    }
    
    public void insertar(E dato){
        Lista<E> nuevo = new Lista<>(dato, null);
        if(Vacia()){
          cabecera = nuevo;  
            
        }else{
            Lista<E> aux = cabecera;
            while(aux.getSiguiente()!=null){
                aux = aux.getSiguiente();
            }
            aux.setSiguiente(nuevo);
        }
        size++;
    }
    public void imprimir(){
        Lista <E> aux = cabecera;
        for(int i=0; i < getSize(); i++){
            System.out.println(aux.getDato().toString());
            aux= aux.getSiguiente();
        }
    }
}
